% Wesley Ellington
% Numerical Analysis
% Homework 2

clc, clear;

%% Problem 4

fprintf('Problem 4\n')

% Define functions to evaluate
ga = @(x) (1/4)*x^2 - (1/2)*x - 1;
gb = @(x) cos(x);
gc = @(x) (1/2)*(3/x + x);
gd = @(x) cosh(x)/x - atan(x);

% Define initial guesses
guesses = [2, 2, 2, 2];
gfuncs = {ga, gb, gc, gd};

% Max number of iterations
maxits = 100;

% Set tolerances
atol = 1e-10;
rtol = 1e-5;

% Iterate over functions and guesses to evaluate
for count = 1 : length(gfuncs)
    % Get function and value pair for this test
    g = gfuncs{count};
    x0 = guesses(count);
    fprintf('\n=====================================================\n')
    fprintf('Starting fixed point solvers with Gfunc:%3i and x0: %g \n', ... 
            count, x0)
    fprintf('=====================================================\n')
    
    % Run Fixed Point solver
    fprintf('\nFixed Point\n');
    fixed_point(g, x0, maxits, rtol, atol, true);
    
    % Run Aitken Extrapolation Solver
    fprintf('\nAitken Extrapolation\n');
    aitken_fp(g, x0, maxits, rtol, atol, true);
end

fprintf('\nAll functions are behaving as expected, and given enough time. \n')
fprintf('This is only an issue for the last function, where the fixed point\n')
fprintf('is never found by the normal f.p. iteration form. This is because\n')
fprintf('the initial guess fo two is outside of converging  space, so no \n')
fprintf('root can be found using a normal iteration. This can be seen by looking\n')
fprintf('at the derivative of g in the area, where it is no less than one, \n')
fprintf(' thus there is not root in the area to converge to. Aitken however, uses \n')
fprintf('a linear model of the three nearest points to construct a next guess\n')
fprintf('and gets lucky, landing close enough to continue to the next iteration.\n')
