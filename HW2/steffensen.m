function [ x, its ] = steffensen( Ffun, x, maxit, Srtol, Satol, Rrtol, Ratol, output )
%STEFFENSEN Implementation of Steffensen's method for root finding of
%nonlinear equations. Results will be returned when the following
%conditions are met:
%        |xnew - xold| < Satol + Srtol*|xnew|
%                           or
%        |f(xnew)| < Ratol + Rrtol*|f(x0)|
%
%   USAGE:
%       [ x, its ] = steffensen( Ffun, x, maxit, Srtol, Satol, Rrtol, Ratol, output )
%   INPUTS:
%       Ffun - function to evaluate
%       x - initial guess for root
%       maxit - Maximum number of iterations for solve
%       Srtol - Solution relative tolerance
%       Satol - Solution absolute tolerance
%       Rrtol - Risidual relative tolerance
%       Ratol - Risidual absolute tolerance
%       output - Turns on or off printed iteration history (1 for on, 0 for off)
%   OUTPUTS:
%       x - approximate solution as calculated
%       its - number of iterations used 


% Input Argument Checks:

% Legal number of iterations
if (floor(maxit) < 1) 
   fprintf('steffensen: maxit = %i < 1. Resetting to 10\n',...
           floor(maxit)); 
   maxit = 10;
end
% Check solution relative tolerance
if (Srtol < 10*eps)
   fprintf('steffensen: Srtol = %g < %g. Resetting to %g\n',...
           Srtol, 10*eps, 10*eps)
   Srtol = 10*eps;
end
% Check solution absolute tolerance
if (Satol < 0)
   fprintf('steffensen: Satol = %g < 0. Resetting to %g\n',...
           Satol, 1e-15)
   Satol = 1e-15;
end
% Check residual relative tolerance
if (Rrtol < 10*eps)
   fprintf('steffensen: Rrtol = %g < %g. Resetting to %g\n',...
           Srtol, 10*eps, 10*eps)
   Rrtol = 10*eps;
end
% Check residual absolute tolerance 
if (Ratol < 0)
   fprintf('steffensen: Ratol = %g < 0. Resetting to %g\n',...
           Ratol, 1e-15)
   Ratol = 1e-15;
end

% Begin calculations
% Set old value of x to initial guess
xold = x;

% Set initial value for tolerance checks
f0 = feval(Ffun, x);
fx = f0;
fxfx = feval(Ffun, (xold + fx));

converged = false;
% Begin iterating to max number of its
for count = 1 : maxit 
    % Calculate Denominator
    g =  (fxfx -fx) / fx;
    % Calculate update Distance
    h = fx / g;
    % Calculate new x
    x = x - h;
    
    % Update f(x) and f(x + f(x))
    fx = feval(Ffun, x);
    fxfx = feval(Ffun, x + fx);
    
    % Output step history
    if output == true;
        fprintf('\titer: %3i \t |h|: %g \thtol: %g \t|f|: %g \tftol: %g \n'...
                , count, abs(h), ...
                Satol + Srtol*abs(x), ... 
                abs(fx), Ratol + Rrtol*abs(f0))
        
    end
    
    % Check for convergence
    if ((abs(h) < Satol + Srtol*abs(x)) || (abs(fx) < Ratol + Rrtol*abs(f0)))
      converged = true;
      its = count;
      if output == true
        fprintf('\tConverged to: %g\n', x);
      end
      break;
    end
    
end

if(converged == false && output == true)
    fprintf('\tIterations did not converge!\n');
end
end % End function

