function [ x, its ] = fixed_point( Gfun, x, maxit, rtol, atol, output )
%FIXED_POINT Basic fixed point iteration function for solving non linear
%equations. Results will be returned when the following conditions are met:
%        |g(x) ? x| < atol + rtol|x|
%
%   USAGE:
%       [ x, its ] = fixed_point( Gfun, x, maxit, rtol, atol, output )
%   INPUTS:
%       Gfun - function to evaluate
%       x - initial guess for root
%       maxit - Maximum number of iterations for solve
%       rtol - Relative tolerance
%       atol - Absolute tolerance
%       output - Turns on or off printed iteration history (1 for on, 0 for off)
%   OUTPUTS:
%       x - approximate solution as calculated
%       its - number of iterations used 

% Input Argument Checks:

% Legal number of iterations
if (floor(maxit) < 1) 
   fprintf('fixed_point: maxit = %i < 1. Resetting to 10\n',...
           floor(maxit)); 
   maxit = 10;
end
% Check solution relative tolerance
if (rtol < 10*eps)
   fprintf('fixed_point: Srtol = %g < %g. Resetting to %g\n',...
           rtol, 10*eps, 10*eps)
   rtol = 10*eps;
end
% Check solution absolute tolerance
if (atol < 0)
   fprintf('fixed_point: Satol = %g < 0. Resetting to %g\n',...
           atol, 1e-15)
   atol = 1e-15;
end

% Set convergence flag for error output
converged = false;

% Begin iterations
for count = 1 : maxit
   % Evaluate g(x) at x
   x = feval(Gfun, x);
   
   % Print iteration information
   if output == true
       fprintf('\titer: %3i \t|g(x)-x|: %g \tatol + rtol|x|: %g\n'...
                , count, abs(feval(Gfun, x) -x),atol + rtol*abs(x));        
   end
   
   % Check for convergence
   if(abs(feval(Gfun, x) - x) < atol + rtol*abs(x))
        if output == true
            fprintf('\tConverged to: %g\n', x);
        end
        
        % Set convergence flag and update its to current counter
        converged = true;
        its = count;
        break;
   end
end

% Display error if no convergence occurs
if(converged == false && output == true)
   fprintf('\tIterations did not converge!\n');
   fprintf('\tValue at final iteration: %g\n', x);
end
end % End Function

