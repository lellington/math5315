function [ x, its ] = aitken_fp( Gfun, x, maxit, rtol, atol, output )
%Aitken Fixed Point Basic fixed point iteration function for solving non linear
%equations by way of Aitken Extrapolation. Results will be returned when 
% the following conditions are met:
%        |g(x) - x| < atol + rtol|x|
%
%   USAGE:
%       [ x, its ] = aitken_fp( Gfun, x, maxit, rtol, atol, output )
%   INPUTS:
%       Gfun - function to evaluate
%       x - initial guess for root
%       maxit - Maximum number of iterations for solve
%       rtol - Relative tolerance
%       atol - Absolute tolerance
%       output - Turns on or off printed iteration history (1 for on, 0 for off)
%   OUTPUTS:
%       x - approximate solution as calculated
%       its - number of iterations used 

% Input Argument Checks:

% Legal number of iterations
if (floor(maxit) < 1) 
   fprintf('fixed_point: maxit = %i < 1. Resetting to 10\n',...
           floor(maxit)); 
   maxit = 10;
end
% Check solution relative tolerance
if (rtol < 10*eps)
   fprintf('fixed_point: Srtol = %g < %g. Resetting to %g\n',...
           rtol, 10*eps, 10*eps)
   rtol = 10*eps;
end
% Check solution absolute tolerance
if (atol < 0)
   fprintf('fixed_point: Satol = %g < 0. Resetting to %g\n',...
           atol, 1e-15)
   atol = 1e-15;
end

% Set convergence flag for error output and set initial condition
converged = false;
xold = x;

% Begin iterations
for count = 1 : maxit
   %Calculate points xk and xk+1
   y = feval(Gfun, xold);
   z = feval(Gfun, y);
   
   % Create new guess for x based on r formula
   xnew = (z * xold - y^2)/ (xold - 2 * y + z);
   
   % Print iteration information
   if output == true
       fprintf('\titer: %3i \t|xnew-x|: %g \tatol + rtol|x|: %g\n'...
                , count, abs(xnew -xold),atol + rtol*abs(xnew));        
   end
   
   % Check for convergence
   if abs(xnew - xold) < (atol + rtol*abs(xnew))
        x = xnew;
        if output == true
            fprintf('\tConverged to %g\n', x);
        end
        % Update convergence flag
        converged = true;
        break;
   end
   
   xold = xnew;
  
end

% Display error if no convergence occurs
if(converged == false && output == true)
   fprintf('\tIterations did not converge!\n');
   fprintf('\tValue at final iteration: %g\n', xnew);
end
end % End Function

