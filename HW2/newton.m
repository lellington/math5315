function [x,its] = newton(Ffun, DFfun, x, maxit, Srtol, Satol, Rrtol, Ratol, output)
% Usage: [x,its] = newton(Ffun, DFfun, x, maxit, Srtol, Satol, Rrtol, Ratol, output)
%
% This routine uses the Newton method to approximate a root of
% the nonlinear system of equations f(x)=0.  The iteration ceases 
% when one of the following conditions is met:
%
%    ||xnew - xold|| < Satol + Srtol*||xnew||
% or
%    ||f(xnew)|| < Ratol + Rrtol*||f(x0)||
%
% inputs:   Ffun     nonlinear function name
%           DFfun    derivative of nonlinear function
%           x        initial guess at solution
%           maxit    maximum allowed number of iterations
%           Srtol    relative solution tolerance
%           Satol    absolute solution tolerance
%           Rrtol    relative residual tolerance
%           Ratol    absolute residual tolerance
%           output   flag (true/false) to output iteration history
% outputs:  x        approximate solution
%           its      number of iterations taken
%
% D.R. Reynolds
% Math5315 @ SMU
% Fall 2017
% With slight modification by Wesley Ellington

% check input arguments
if (floor(maxit) < 1) 
   fprintf('newton: maxit = %i < 1. Resetting to 10\n',...
           floor(maxit)); 
   maxit = 10;
end
if (Srtol < 10*eps)
   fprintf('newton: Srtol = %g < %g. Resetting to %g\n',...
           Srtol, 10*eps, 10*eps)
   Srtol = 10*eps;
end
if (Satol < 0)
   fprintf('newton: Satol = %g < 0. Resetting to %g\n',...
           Satol, 1e-15)
   Satol = 1e-15;
end
if (Rrtol < 10*eps)
   fprintf('newton: Rrtol = %g < %g. Resetting to %g\n',...
           Srtol, 10*eps, 10*eps)
   Rrtol = 10*eps;
end
if (Ratol < 0)
   fprintf('newton: Ratol = %g < 0. Resetting to %g\n',...
           Ratol, 1e-15)
   Ratol = 1e-15;
end

% evaluate initial residual
f = feval(Ffun,x);
f0norm = norm(f);

converged = false;

% begin iteration
for its=1:maxit

   % evaluate derivative
   Df = feval(DFfun,x);

   % compute Newton update, new guess at solution, new residual
   h = Df\f;
   x = x - h;
   f = feval(Ffun,x);

   % check for convergence and output diagnostics
   hnorm = norm(h);
   xnorm = norm(x);
   fnorm = norm(f);
   if (output)
      fprintf('\titer %3i, \t||h|| = %g, \thtol = %g, \t||f|| = %g, \tftol = %g\n',...
              its, hnorm, Satol + Srtol*xnorm, fnorm, Ratol + Rrtol*f0norm);
   end
   if ((hnorm < Satol + Srtol*xnorm) || (fnorm < Ratol + Rrtol*f0norm))
      if output == true
        fprintf('\tConverged to: %g\n', x);
      end
      converged = true;
      break;
   end
   
   
end
if(converged == false && output == true)
       fprintf('\tIterations did not converge!\n');
end
% end of function
