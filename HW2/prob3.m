% Wesley Ellington
% Numerical Analysis
% Homework 2

clc, clear;

%% Problem 3

fprintf('Problem 3\n');

% Define function to evaluate
f = @(x) exp(x/2) - 3*x - 2;
% Derivative for newton iterations
fprime = @(x) (1/2) * exp(x/2) - 3;

% Set tolerances
Srtol = 1e-6;
Satol = 1e-6;
Rrtol = 1e-13;
Ratol = 1e-13;

% Max number of iterations
maxits = 50;

% Initial guesses to try
guesses = [-5, 2, 5];

for count = 1 : length(guesses)
    
    x0 = guesses(count);   

    fprintf('\n======================================\n')
    fprintf('Starting Calculation with x0 = %g\n',x0);
    fprintf('======================================\n')
    fprintf('Steffensen: \n');
    steffensen(f, x0, maxits, Srtol, Satol, Rrtol, Ratol, true);
    fprintf('Newton: \n')
    newton(f, fprime, x0, maxits, Srtol, Satol, Rrtol, Ratol, true);
end

fprintf('\nThe convergence of each function here is about what we expect for\n')
fprintf('such low iteration counts, so it hard to determine c exactly, but\n')
fprintf('Looks quadratice for all cases except Steffensens method for guess\n')
fprintf(' -5. The best explaination of this comes from the fact that f(x + f(x))\n')
fprintf('is poorly behaving at this point. Should more iterations be allowed, \n')
fprintf('we converge to somewhere around -19, but this is not a root. It seems\n')
fprintf('that this is due to the fact that the approximation of fprime is very\n')
fprintf('bad becuase |f(x)| stays large throughout.\n')