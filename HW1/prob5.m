% Wesley Ellington
% Homework 1
% Problem 5

% Demonstrate the following functions are of the specified order

% Every function shall be run for a specified number of values of n to see
% properties that arise as n become sufficiently large

% C plots are derived from the ratio of the order function and the actual
% function for each point

% Emergent properties of C will be shown for very high N at the end of each
% block

% Values are plotted on a logarithmic scale to make the difference between
% functions more evident
clc
clear
%% Part A
% (n + 1)/(n^2) = O(1/n)
clear

% Number of iterations observed
num_tests =  100;

% Distance between points
n_max = 100;

% Vector of points to be evaluated
n_vals = linspace(0, n_max, num_tests);

% Value of actual function
f_n = zeros(1, num_tests);

% Value of order function
o_n = zeros(1, num_tests);

% Value of C approach
c_n = zeros(1, num_tests);

% Generate out puts for each step
n = 0;
for i = 1:num_tests
    n = n_vals(1,i);
    f_n(1,i) = (n + 1)/(n^2);
    o_n(1,i) = (1/n);
    c_n(1,i) = f_n(1,i) / o_n(1,i);
end

% Plot Results
figure('Name','Part A: O(1/N) Convergence','NumberTitle','off')
semilogy(n_vals, f_n, n_vals, o_n, n_vals, c_n);
legend('f(x)', 'O(1/n)', 'C')

% Calculate C for very high N
N = 1000000000;
c = (N + 1)/(N^2) / (1/N);

% Display C Limit
fprintf('Part A\n')
fprintf('C approaching %f at N = %d\n', c, N)
fprintf('C Converges to constant value\n')

%% Part B
% 1/(n * ln(n)) = o(1/n)
clear

% Number of iterations observed
num_tests =  100;

% Max Value reached
n_max = 100;

% Vector of points to be evaluated
n_vals = linspace(1, n_max, num_tests);

% Value of actual function
f_n = zeros(1, num_tests);

% Value of order function
o_n = zeros(1, num_tests);

% Value of C approach
c_n = zeros(1, num_tests);

% Generate out puts for each step
n = 0;
for i = 1:num_tests
    n = n_vals(1,i);
    f_n(1,i) = 1/(n * log(n));
    o_n(1,i) = (1/n);
    c_n(1,i) = f_n(1,i) / o_n(1,i) ;
end

% Plot Results
figure('Name','Part B: o(1/N) Convergence ','NumberTitle','off')
semilogy(n_vals, f_n, n_vals, o_n, n_vals, c_n);
legend('f(x)', 'o(1/n)', 'C')

% Calculate C for very high N
N = 1000000000000000;
c = 1/(N * log(N)) / (1/N);

% Display C Limit
fprintf('Part B\n')
fprintf('Cn still decreasing  (%f at N = %d)\n', c, N)
fprintf('Cn decreases with higher values of N\n')

%% Part C
% 1/n = o(1/ln(n))
clear

% Number of iterations observed
num_tests =  100;

% Max Value reached
n_max = 100;

% Vector of points to be evaluated
n_vals = linspace(1, n_max, num_tests);

% Value of actual function
f_n = zeros(1, num_tests);

% Value of order function
o_n = zeros(1, num_tests);

% Value of C approach
c_n = zeros(1, num_tests);

% Generate out puts for each step
n = 0;
for i = 1:num_tests
    n = n_vals(1,i);
    f_n(1,i) = 1/n;
    o_n(1,i) = (1/log(n));
    c_n(1,i) = f_n(1,i) / o_n(1,i);
end

% Plot Results
figure('Name','Part C: o(1/ln(N)) Convergence ','NumberTitle','off')
plot(n_vals, f_n, n_vals, o_n, n_vals, c_n);
legend('f(x)', 'o(1/ln(n))', 'C')

% Calculate C for very high N
N = 1000000;
c = 1/(N) / (1/log(N));

% Display C Limit
fprintf('Part C\n')
fprintf('Cn still decreasing (%f at N = %d)\n', c, N)
fprintf('Cn decreases with higher values of N => o()\n')

%% Part D
% 5/n * e^-n = O(1/n)
clear

% Number of iterations observed
num_tests =  100;

% Max Value reached
n_max = 100;

% Vector of points to be evaluated
n_vals = linspace(1, n_max, num_tests);

% Value of actual function
f_n = zeros(1, num_tests);

% Value of order function
o_n = zeros(1, num_tests);

% Value of C approach
c_n = zeros(1, num_tests);

% Generate out puts for each step
n = 0;
for i = 1:num_tests
    n = n_vals(1,i);
    f_n(1,i) = 5/n + exp(-n);
    o_n(1,i) = (1/n);
    c_n(1,i) = f_n(1,i) / o_n(1,i);
end

% Plot Results
figure('Name','Part D: O(1/N) Convergence ','NumberTitle','off')
semilogy(n_vals, f_n, n_vals, o_n, n_vals, c_n);
legend('f(x)', 'O(1/n)', 'C')

% Calculate C for very high N
N = 1000000;
c = (5/N + exp(-N)) / (1/(N));

% Display C Limit
fprintf('Part D\n')
fprintf('Cn converges toward %f at N = %d\n', c, N)
fprintf('Cn convergent to constant\n')

%% Part E
% e^-n = o(1/n^2)
clear

% Number of iterations observed
num_tests =  100;

% Max Value reached
n_max = 100;

% Vector of points to be evaluated
n_vals = linspace(1, n_max, num_tests);

% Value of actual function
f_n = zeros(1, num_tests);

% Value of order function
o_n = zeros(1, num_tests);

% Value of C approach
c_n = zeros(1, num_tests);

% Generate out puts for each step
n = 0;
for i = 1:num_tests
    n = n_vals(1,i);
    f_n(1,i) = exp(-n);
    o_n(1,i) = (1/(n^2));
    c_n(1,i) = f_n(1,i) / o_n(1,i) ;
end

% Plot Results
figure('Name','Part E: o(1/n^2) Convergence ','NumberTitle','off')
semilogy(n_vals, f_n, n_vals, o_n, n_vals, c_n);
legend('f(x)', 'o(1/ln(n))', 'C')

% Calculate C for very high N
N = 100000;
c = (exp(-N)) / (1/(N^2));

% Display C Limit
fprintf('Part E\n')
fprintf('Cn still decreasing (%f at N = %d)\n', c, N)
fprintf('Cn decreasing series => o()\n')
