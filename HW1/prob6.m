% Wesley Ellington
% Homework 1
% Problem 6

% In this file, we will look at the nature of floating point machine
% numbers to better understand roundoff (epsilon), maximum, and minimum for
% single and double precission floating point numbers.

clear
clc

%% Part A
fprintf('Part A\n')
% Determine the machine precision epsilon for both single and double
% precision

% Set long format print param
format long

% Initialize Values for epsilon check
ep_single = single(1);
one_single = single(1);

% Maximum number of iterations
max_its = 10000;

% Loop until max value
for i = 1 : max_its
   % Print ep if less than machine ep
   if ep_single + one_single == one_single
       % Scale up by factor of two if found
       ep_single = ep_single * 2
       break
   end  
   % Divide e by two
   ep_single = ep_single / 2;
end

% Initialize Values for epsilon check
ep_double = double(1);
one_double = double(1);

max_its = 10000;

% Loop until max value
for i = 1 : max_its
   % Print ep if less than machine ep
   if ep_double + one_double == one_double
       % Scale up by factor of two if found
       ep_double = ep_double * 2
       break
   end
   % Divide e by two
   ep_double = ep_double / 2;
end

%% Part B
fprintf('Part B\n')
% Determine largest available value for single and double precision values

% Maximum value
initial_single = single(1);
max_single = single(0);
old_max = single(0);

% Max number of iterations
max_its = 10000;

for i = 1 : max_its
   initial_single = initial_single * 2;
   
   % If value has reached inf, report previous result
   if initial_single == max_single
       % Scale by machine ep for better approximation   
       max_single  = (old_max * (1 - ep_single)) * 2
       break
   end
   % Update iteration variables
   old_max = max_single;
   max_single = initial_single;
end


% Maximum value
initial_double = double(2);
max_double = double(0);
old_max = double(0);

% Max number of iterations
max_its = 10000;

for i = 1 : max_its
   initial_double = initial_double * 2;
   
   %If Value has reached inf, report previous result
   if initial_double == max_double
       % Scale by machine ep to get better approximation
       max_double = (old_max * (1 - ep_double)) * 2
       break
   end
   % Update Iteration variables
   old_max = max_double;
   max_double = initial_double;
end


%% Part C
fprintf('Part C\n')
% Find the smallest positive value representable by single and double 
% precision 


% Note that these values are the minimum possible SUBNORMAL values
% expressable by floating point. This means that the realmin() function
% will not yeild the same result, as realmin gives the smallest NORMAl
% value. For more information, visit: 
% https://docs.oracle.com/cd/E19957-01/806-3568/ncg_math.html

% Single Precision
min_initial = single(1);
min_single = 0;

max_its = 1000;


for i = 0: max_its
   if min_initial == 0
       min_single = min_single 
       break
   end
   min_single = min_initial;
   min_initial = min_initial /2;
end

% Double Precision
min_initial = double(1);
min_double = 0;

max_its = 10000;


for i = 0: max_its
   if min_initial == 0
       min_double = min_double 
       break
   end
   min_double = min_initial;
   min_initial = min_initial /2;
end